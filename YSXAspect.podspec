Pod::Spec.new do |s|
    s.name         = 'YSXAspect'
    s.version      = '1.0.5'
    s.license      = 'MIT'
    s.summary      = 'Making code maintainable and reusable with aspect-oriented programming for Objective-C'
    s.homepage     = 'https://bitbucket.org/TY_iOS_Librarys/ysxaspect.git'
    s.authors      = { 'yans' => 'aoyan2010@live.cn' }
    s.source       = { :git => "https://bitbucket.org/TY_iOS_Librarys/ysxaspect.git", :tag => s.version.to_s, :submodules =>  true }

    s.requires_arc = false

    s.ios.deployment_target = '7.0'
    s.osx.deployment_target = '10.7'

    s.library = 'c++'
    s.pod_target_xcconfig = {
        #'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
        "CLANG_CXX_LANGUAGE_STANDARD" => "gnu++0x",
        'CLANG_CXX_LIBRARY' => 'libc++',
    }

    s.public_header_files = 'YSXAspect/XAspect.h'
    s.source_files = 'YSXAspect/XAspect.h'
    s.default_subspecs = 'Core', 'Macros'

    s.subspec 'Macros' do |ss|
        ss.source_files = 'YSXAspect/Macros/*.h'
    end

    s.subspec 'Core' do |ss|
        ss.source_files = 'YSXAspect/Core/*.{h,m,mm,c,cpp}'
        ss.dependency 'YSXAspect/Macros'
    end

end

