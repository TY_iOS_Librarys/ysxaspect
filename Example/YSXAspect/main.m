//
//  main.m
//  YSXAspect
//
//  Created by 478356182@qq.com on 02/11/2018.
//  Copyright (c) 2018 478356182@qq.com. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
