# YSXAspect

[![CI Status](http://img.shields.io/travis/478356182@qq.com/YSXAspect.svg?style=flat)](https://travis-ci.org/478356182@qq.com/YSXAspect)
[![Version](https://img.shields.io/cocoapods/v/YSXAspect.svg?style=flat)](http://cocoapods.org/pods/YSXAspect)
[![License](https://img.shields.io/cocoapods/l/YSXAspect.svg?style=flat)](http://cocoapods.org/pods/YSXAspect)
[![Platform](https://img.shields.io/cocoapods/p/YSXAspect.svg?style=flat)](http://cocoapods.org/pods/YSXAspect)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YSXAspect is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YSXAspect'
```

## Author

478356182@qq.com, 478356182@qq.com

## License

YSXAspect is available under the MIT license. See the LICENSE file for more info.
